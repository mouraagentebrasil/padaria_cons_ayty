<?php
include 'config.php';
ini_set('display_errors', true);
ini_set('display_startup_errors', true);
error_reporting(E_ALL);

if(isset($_GET['action']) && $_GET['action']!="download"){
	$_GET['action']($_GET['datei'],$_GET['datef']);
} elseif(isset($_GET['action'])=="download"){
	header('Location: /csvs/'.$_GET['download']);
	exit;
}

function getStatusLeads($datei,$datef) {
	global $con;
	$sql = "SELECT 
	ML.ID_MAILING, ML.CODE_MAILING_CLIENT,
	S.NM_STATUS, ML.NM_MAILING,ML.DE_EMAIL, 
	CONVERT(VARCHAR(19),ML.DT_REGISTER,121),
	CI.VL_MONTHLY_TOTAL, P.NM_PRODUCT
	FROM MAILING_LOAD ML
	JOIN MAILING M ON M.ID_MAILING = ML.ID_MAILING
	JOIN STATUS S ON S.ID_STATUS = M.ID_STATUS
	JOIN CUSTOMER_ITEM CI ON CI.ID_MAILING = M.ID_MAILING
	JOIN PRODUCT P ON P.ID_PRODUCT = CI.ID_PRODUCT
	WHERE M.ID_CAMPAIGN NOT IN (1) AND S.ID_STATUS = 500
	AND ML.DT_REGISTER BETWEEN '".$datei." 00:00:00' AND '".$datef." 23:59:59'";
	//echo "<pre>";
	$res = mssql_query($sql,$con);
	$csv = "lead_nome;status;lead_id;computado;plano;valor_total\n";
	while($data = mssql_fetch_array($res)){
		$csv.= $data['NM_MAILING'].";".$data['NM_STATUS'].";".$data['CODE_MAILING_CLIENT'].";".$data['computed'].";"
		.$data['NM_PRODUCT'].";".$data['VL_MONTHLY_TOTAL']."\n";
	}
	$fileName = time().".csv";
	file_put_contents("csvs/".$fileName, $csv);
	echo $fileName;
};
