<!DOCTYPE html>
<html>
<head>
	<title>Consultas AYTY</title>
	<script src="js/jquery-3.1.1.min.js"></script>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="js/blockUI.js"></script>
	<script	src="js/Datepicker/datepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="js/Datepicker/datepicker.css">

</head>
<body>	
	<div class="col-md-4"></div>
	<div class="col-md-4">
		<h1>CONSULTAS AYTY</h1>
		<form class="form-horizontal" id="form_busca">
			<label>Ação</label>
			<div class="form-group">
				<select name="action" class="form-control" id="action">
					<option value="getStatusLeads">LEAD STATUS PRÉ-VENDA</option>
				</select>	
			</div>
			<div class="form-group">
				<label>Data Inicial</label>
				<input type="text" id="datei" name="datei" class="form-control">
			</div>
			<div class="form-group">
				<label>Data Final</label>
				<input type="text" id="datef" name="datef" class="form-control">
			</div>
			<div class="form-group">
				<button class="btn btn-warning col-md-12 buscar">DOWNLOAD DO RELATORIO</button>
			</div>
		</form>
		Obs: sempre habilite o pop-up desse site
		<div class="col-md-4"></div>
	</div>
	<script type="text/javascript">
		$('#datei').datepicker({
            "useCurrent": true,
            "setDate": new Date(),
            "autoclose": true,
            "language": 'pt',
            "format": "yyyy-mm-dd"
        });

        $('#datef').datepicker({
            "useCurrent": true,
            "setDate": new Date(),
            "autoclose": true,
            "language": 'pt',
            "format": "yyyy-mm-dd"
        });

		$('#form_busca').on('submit',function(e){
			e.preventDefault();
			var datei = $('#datei').val();
			var datef = $('#datef').val();
			var option = $('#action').val();
			var url = "/bin.php?action="+option+"&datei="+datei+"&datef="+datef;
			var promise = $.ajax({'method':'get',url: url});
			$.blockUI({'message':'Processando...'});
			promise.done(function(rest){
				$.unblockUI();
				window.open('/bin.php?action=download&download='+rest);
			});

			promise.fail(function(){
				$.unblockUI();
			})
		});
	</script>
</body>
</html>